import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    width: 250px;
    height: 390px;
    background-color: #fff;
    padding: 10px;
    margin: 10px;

    div {
        margin: 10px;

        img {
            height: 200px;
            max-width: 250px    ;
        }
    }

    h3 {
        margin: 5px 0;
    }

    p {
        margin: 5px 0;
    }
`