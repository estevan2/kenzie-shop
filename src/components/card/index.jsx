import { Button } from "@material-ui/core"
import { useDispatch } from "react-redux"
import { addToCartThunk } from "../../store/module/cart/thunks"
import { Container } from "./style"

const Card = ({ image, name, price, product }) => {

    const dispatch = useDispatch()
    const handleClick = () => {
        dispatch(addToCartThunk(product))
    }

    return (
        <Container>
            <div>
                <img src={image} alt="" />
            </div>
            <h3>{name}</h3>
            <p>R$ {price}</p>
            <Button
                onClick={handleClick}
                variant="contained"
                color="primary"
            >
                Adicionar ao carrinho
            </Button>
        </Container>
    )
}

export default Card