import { useSelector } from "react-redux"
import { useHistory } from "react-router-dom"
import { Container } from "./style"

const Head = () => {

    const history = useHistory()
    const cartNumber = useSelector(state => state.cart).length

    return (
        <Container>
            <h1 onClick={() => history.push("/")}>Kenzie Shop</h1>
            <p onClick={() => history.push("/cart")}>
                {cartNumber > 0 ?
                    <span>
                        {cartNumber}
                    </span> : null
                }
                Carrinho
            </p>
        </Container>
    )
}

export default Head