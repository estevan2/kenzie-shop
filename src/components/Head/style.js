import styled from "styled-components";

export const Container = styled.header`
    display: flex;
    justify-content: space-between;
    height: 50px;
    border-bottom: 1px solid #cccccc;
    align-items: center ;

    h1 {
        font-weight: inherit;
        cursor: pointer;
        margin-left: 10px
    }
    
    p {
        cursor: pointer;
        margin-right: 10px;

        span {
            background-color: #3f51b5;
            color: #fff;
            border-radius: 50%;
            padding: 1px 6px;
            margin-right: 5px;
        }
    }
`