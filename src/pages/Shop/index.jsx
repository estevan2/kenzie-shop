import { useSelector } from "react-redux"
import Card from "../../components/card"
import { Container } from "./style"

const Shop = () => {

    const products = useSelector(state => state.productsList)

    return (
        <Container>
            {products.map((product, index) => <Card
                key={index}
                image={product.image}
                name={product.name}
                price={product.price}
                product={product}
            />)}
        </Container>
    )
}

export default Shop