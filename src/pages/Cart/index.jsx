// import { Button } from "@material-ui/core"
import { useDispatch, useSelector } from "react-redux"
import { subToCartThunk } from "../../store/module/cart/thunks"
import { Container } from "./style"
import { FiTrash2 } from 'react-icons/fi'

const Cart = () => {

    const cart = useSelector(state => state.cart)
    const dispatch = useDispatch()
    const handleClick = id => {
        dispatch(subToCartThunk(id))
    }

    return (
        <Container>
            <div className="finalizeProduct">
                <p>Resumo do Pedido</p>
                <p>{cart.length}   - R${cart.reduce((acc, item) => item.price + acc, 0).toFixed(2)}</p>
                {/* <Button variant="contained" color="primary">Finalizar o Pedido</Button> */}
            </div>
            <div className="productsList">
                <header>
                    <p>Produto</p>
                    <p>Preço</p>
                </header>
                <div className="products">
                    {cart.map((item, index) => (
                        <div key={index}>
                            <button onClick={() => handleClick(item.id)}><FiTrash2 /></button>
                            <div>
                                <img src={item.image} alt={item.name} />
                            </div>
                            <p>{item.name}</p>
                            <p>R$ {item.price}</p>
                        </div>
                    ))}
                </div>
            </div>
        </Container>
    )
}

export default Cart