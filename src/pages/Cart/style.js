import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    flex-direction: row-reverse;
    justify-content: space-evenly;
    margin-top: 10px;

    .finalizeProduct {
        display: flex;
        flex-direction: column;
        justify-content: space-evenly;
        align-items: center;
        background-color: #fff;
        width: 200px;
        height: 200px;
        border-radius: 5px;
    }

    .productsList {
        display: flex;
        flex-direction: column;
        width: 600px;
        background-color: #fff;
        padding: 10px;
        border-radius: 10px;

        header {
            display: flex;
            justify-content: space-between;
            margin-bottom: 10px;
        }

        .products {
            width: 100%;

            div {
                display: flex;
                width: 100%;
                justify-content: space-between;
                margin-top: 10px;
                position: relative;

                button {
                    position: absolute;
                    top: 0;
                    left: 0;
                    width: 30px;
                    height: 30px;
                    border: none;
                    border-radius: 5px;
                    background-color: #d90000;
                    cursor: pointer;
                    z-index: 1;

                    :hover {
                        filter: brightness(1.1);
                    }
                }

                div {
                    display: flex;
                    justify-content: center;
                    height: 200px;
                    width: 150px;


                    img {
                        max-width: 100%;
                    }
                }
            }
        }
    }
`