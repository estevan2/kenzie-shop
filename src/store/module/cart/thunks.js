import { addToCart, subToCart } from "./actions";

export const addToCartThunk = (product) => {
    return (dispatch, getState) => {
        const { cart } = getState()
        const updatedProductsToCart = [...cart, product]
        dispatch(addToCart(updatedProductsToCart))
    }
}

export const subToCartThunk = (id) => {
    return (dispatch, getState) => {
        const { cart } = getState()
        const newCartList = cart.filter(item => item.id !== id)
        dispatch(subToCart(newCartList))
    }
}