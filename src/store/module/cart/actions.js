import { ADD_TO_CART, SUB_TO_CART } from "./actionTypes";

export const addToCart = (updatedProductsToCart) => ({
    type: ADD_TO_CART,
    updatedProductsToCart,
})

export const subToCart = (updatedProductsToCart) => ({
    type: SUB_TO_CART,
    updatedProductsToCart,
})