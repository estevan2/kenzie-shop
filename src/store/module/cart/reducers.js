import { ADD_TO_CART, SUB_TO_CART } from "./actionTypes";

const cartReducer = (state = [], action) => {
    switch (action.type) {
        case ADD_TO_CART:
            return action.updatedProductsToCart
        case SUB_TO_CART:
            return action.updatedProductsToCart
        default:
            return state
    }
}

export default cartReducer