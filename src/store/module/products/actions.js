import { ADD_PRODUCT } from "./actionTypes";

export const addProduct = (updatedProductsList) => ({
    type: ADD_PRODUCT,
    updatedProductsList,
})