import { ADD_PRODUCT } from "./actionTypes";

const initialProductsList = [
    { id: 1, name: "Echo Dot", price: 279.00, image: "https://m.media-amazon.com/images/I/41GZCWFJB1L._AC_SX466_.jpg" },
    { id: 2, name: "Kindle Paperwhite", price: 499.00, image: "https://m.media-amazon.com/images/I/61ldUg+PqQL._AC_SY450_.jpg" },
    { id: 3, name: "Notebook Lenovo Ultrafino IdeaPad 3i", price: 3599.91, image: "https://m.media-amazon.com/images/I/61MNnJN1YQL._AC_SY355_.jpg" },
    { id: 4, name: "Smartphone Xiaomi Redmi Note 8", price: 1165.99, image: "https://m.media-amazon.com/images/I/51Fv-PIiDQL._AC_SX522_.jpg" },
    { id: 5, name: "Novo Apple iPad Space Gray 8ª geração", price: 2541.00, image: "https://m.media-amazon.com/images/I/71gOkVA6-eL._AC_SX679_.jpg" },
]

const productReducer = (state = initialProductsList, action) => {
    switch (action.type) {
        case ADD_PRODUCT:
            return action.updatedProductsList
        default:
            return state
    }
}

export default productReducer