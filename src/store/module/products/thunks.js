import { addProduct } from "./actions";

export const addProductThunk = (product) => {
    return (dispatch, getStore) => {
        const { productsList } = getStore()
        const updatedProductsList = [...productsList, product]
        dispatch(addProduct(updatedProductsList))
    }
}