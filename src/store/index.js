import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from "redux-thunk";
import cartReducer from "./module/cart/reducers";
import productReducer from "./module/products/reducers";

const reducers = combineReducers({ cart: cartReducer, productsList: productReducer })

const store = createStore(reducers, applyMiddleware(thunk))

export default store