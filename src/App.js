import Router from './routes'
import Head from './components/Head'
import GlobalStyle from './styles/global';

function App() {

  return (
    <>
      <Head />
      <Router />
      <GlobalStyle />
    </>
  )
}

export default App;
