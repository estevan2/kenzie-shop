import { Route, Switch } from "react-router-dom"
import Cart from "../pages/Cart"
import Shop from "../pages/Shop"

const Router = () => {

    return (
        <Switch>
            <Route exact path="/">
                <Shop />
            </Route>
            <Route path="/cart">
                <Cart />
            </Route>
        </Switch>
    )
}

export default Router